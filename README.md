# 工程简介
把用户SSL证书上传到阿里云数字证书管理服务，同时也可以把上传的用户证书绑定到阿里云OSS服务。

# 部署说明
## 1.在阿里云RAM控制台创建RAM用户
登录[阿里云RAM控制台](https://ram.console.aliyun.com/users)创建用户，记录用户的AccessKey ID和AccessKey Secret。
并赋予新创建用户AliyunDNSFullAccess、AliyunYundunCertFullAccess、AliyunOSSFullAccess三个权限。
## 2.安装certbot程序，用于申请let's encrypt的SSL证书
详细安装步骤参考[certbot官网的说明](https://certbot.eff.org/instructions)，如果您和我一样使用Ubuntu20.04系统，您可以使用已下命令安装

```shell
sudo snap install --classic certbot
```

## 3.安装certbot-dns-aliyun插件，用于通过DNS认证方式获取SSL证书
安装及配置过程参见[certbot-dns-aliyun](https://github.com/justjavac/certbot-dns-aliyun)的README。
特别说明以下aliyun cli工具的认证方式选AK，AccessKey ID和AccessKey Secret使用第一节创建用户的AccessKey ID和AccessKey Secret。

参考配置命令如下，region参数填OSS所在区域。
```shell
aliyun configure set \
  --mode AK \
  --access-key-id **** \
  --access-key-secret **** \
  --region cn-hangzhou
```
配置完成后，参考[certbot-dns-aliyun](https://github.com/justjavac/certbot-dns-aliyun)的README中的说明申请证书。

## 4.部署cert-uploader项目
下载项目源码，使用Maven打包。
拷贝生成的jar包到服务器用户目录，在jar包所在目录创建config目录，在config目录创建application.yaml文件
application.yaml内容如下，替换yourKeyId、yourSecret为第一节创建用户的AccessKey ID和AccessKey Secret。
```yaml
aliyun:
  accessKeyId: yourKeyId
  accessKeySecret: yourSecret
```

## 5.上传并部署证书

```shell
# 完整指令 java -jar /pathofyourjar/cert-uploader-1.0.jar -n name-$(date +"%Y%m%d" -d "90 days") -d  /pathtoyourcert/*.*.com -k privkey.pem -c fullchain.pem -deploy --domain *.*.com -b bucketname -a upload 
# 参数说明
java -jar /pathofyourjar/cert-uploader-1.0.jar \ # jar包完整路径
-n name-$(date +"%Y%m%d" -d "90 days") \ # 指定证书名称 
-d  /pathtoyourcert/*.*.com \ # 生成证书所在目录
-k privkey.pem \ # 私钥文件名称
-c fullchain.pem  \ # 证书文件名称
-deploy \ # 部署证书
--domain *.*.com \ # oss bucket定义的域名
-b bucketname \ # oss bucket名称
-a upload 
```